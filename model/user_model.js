var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/nerdos');

var userSchema = new mongoose.Schema({
    username: String,
    password: String,
    email: String,
    photo: String
});

var UserModel = mongoose.model('User', userSchema);


module.exports = {
    findById: (id)=>{
        return UserModel.findById(id);
    },
    findAll: () => {
        return UserModel.find();
    },
    save: (username, password, email, photo) => {
        var user = new UserModel({
            username,
            password,
            email,
            photo
        });
        return user.save();
    },
    deleteById : function (id) {
        return UserModel.remove({_id : id});
    }
};
