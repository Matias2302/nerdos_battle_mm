var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var matchSchema = new mongoose.Schema({
    name: String,
    roomType: String,
    userQt: Number,
    userList: [userSchema],
    roomPassword: String,
    cardQt: Number,
    winner: [userSchema]
});

var Match = mongoose.model('Match', matchSchema);
module.exports = Match;
