var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var playSchema = new mongoose.Schema({
    users:[userSchema],
    cards:[cardSchema],
    winner:[userSchema]
});

var Play = mongoose.model('Play', playSchema);
module.exports = Play;
