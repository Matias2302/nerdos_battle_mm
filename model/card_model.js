var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var cardSchema = new mongoose.Schema({
    name: String,
    photo: String,
    kills: [{type: Schema.Types.ObjectID, ref: 'kill'}]
});

var killSchema = Schema ({
    name:
})

var CardModel = mongoose.model('Card', cardSchema);
// module.exports = Card;

module.exports = {
    findById: (id)=>{
        return CardModel.findById(id);
    },
    findAll: () => {
        return CardModel.find();
    },
    save: (name, photo, kills) => {
        var card = new CardModel({
            name,
            photo,
            kills
        });
        return user.save();
    },
    deleteById : function (id) {
        return CardModel.remove({_id : id});
    }
};
