const express = require('express');
const app = express();
const router = require('./router');
var methodOverride = require('method-override')


app.set('view engine', 'ejs');
app.use(methodOverride('_method'))

app.use('/', router);
app.listen(8000);

app.get("/", ()=>{
    console.log()
})
