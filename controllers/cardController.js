const express = require('express');
const users_model = require('../model/card_model');
var router = express.Router();

const bodyParser = require('body-parser');
router.use(bodyParser.urlencoded({extended: false}));

router.get('/', (req, res) => {
    users_model.findAll().then(cards => {
        res.render('pages/cards/index', {cards: cards});
    });
});

router.get('/new', (req, res) => {
    res.render('pages/cards/index' , {cards:cards});
});

router.post('/new', (req, res) => {
    card.save(req.name, '-', req.kills)
    .then(() => {
        res.redirect('/cards');
    });
});


module.exports = router;
