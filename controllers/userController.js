const express = require('express');
const users_model = require('../model/user_model');
var router = express.Router();

const bodyParser = require('body-parser');
router.use(bodyParser.urlencoded({extended: false}));


router.get('/', (req, res) => {
    users_model.findAll().then(users => {
        res.render('pages/user/index', {users: users});
    });
});

router.get('/new', (req, res) => {
    res.render('pages/user/form');
});

router.post('/new', (req, res) => {
    users_model.save(req.body.username, req.body.password, req.body.email, '-')
        .then(() => {
            res.redirect('/users');            
        });
});

router.delete('/:id', (req, res) => {
    users_model.deleteById(req.params.id).then(() => {
        res.redirect('/users');
    });
});


// router.get('/:id', function getUser(req, res) {
//     res.setHeader('Content-Type', 'application/json');
//     console.log(users.findById(req.params.id))
//     users.findById(req.params.id, (err, user) => {
//         // if (err) throw err;
//         res.send(JSON.stringify(user));
//     });
// });
//
// router.post('/', function registerUser(req, res) {
//
//     users.save(req.body.username,
//         req.body.password,
//         req.body.email,
//         '');
//     res.status(200).end();
//
// });

module.exports = router;
