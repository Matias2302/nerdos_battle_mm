const express = require('express');
const fs = require('fs');
var router = express.Router();
router.use('/users', require('./controllers/userController.js'));
router.use('/cards', require('./controllers/cardController.js'));

module.exports = router;
